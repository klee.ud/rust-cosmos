FROM gitpod/workspace-full

COPY --from=ghcr.io/cosmoscontracts/juno:v9.0.0 /usr/bin/junod /usr/bin/
COPY --from=ghcr.io/cosmoscontracts/juno:v9.0.0 /opt/*.sh /opt/

RUN rustup target add wasm32-unknown-unknown
RUN cargo install cargo-generate --features vendored-openssl
RUN cargo install cargo-run-script

