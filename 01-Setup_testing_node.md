Setup testing node
===

Add the `default` account to **keyring** with `junod keys`. The `--recover` option is used to recover keys with known mnemonics.

```sh
junod keys add default --recover
```

When prompted, enter below 24-word mnemonic.

```
clip hire initial neck maid actor venue client foam budget lock catalog sweet steak waste crater broccoli pipe steak sister coyote moment obvious choose
```

Run `junod keys list` to list all the keys in keyring.

```sh
junod keys list
```

You should get something similar. Take note of the `address:` keys, those are the public addresses of the named keypairs. You may ignore the `validator` keypair for now.

```yaml
- name: default
  type: local
  address: juno16g2rahf5846rxzp3fwlswy08fz8ccuwk03k57y
  pubkey: '{"@type":"/cosmos.crypto.secp256k1.PubKey","key":"A9C5c0m1DeTjKyKsba9mvXm/QwYACC+aLs+8q7RA7YCq"}'
  mnemonic: ""
- name: validator
  type: local
  address: juno1ww7mt202dnpcjaxr65pgjcurhklcwprj077s8p
  pubkey: '{"@type":"/cosmos.crypto.secp256k1.PubKey","key":"A38AHx2oIY1/Fgg5cjAfWkck1sb2etOenC+EDsZNX1Ox"}'
  mnemonic: ""
```


> **TIP**💡 The keyring makes it easily to use keypairs using names instead of long addresses when interacting with the node.

> **TIP**💡 DO NOT use the default keypair in live networks to store real assets!

> **TIP**💡 The name `default` is chosen arbitrarily. You can name it however you want as long as it is unique in the keyring. We will refer to this keypair
as `default` throughout this tutorial.


Starting the test node
---

Now start the local node.

```sh
UNSAFE_CORS=true junod start --rpc.laddr tcp://0.0.0.0:26657 --trace
```

You should see something similar.

```
...
7:53AM INF indexed block height=4 module=txindex
7:53AM INF Timed out dur=4989.720495 height=5 module=consensus round=0 step=1
7:53AM INF received proposal module=consensus proposal={"Type":32,"block_id":{"hash":"A03146F88054588E4AC49B14C7D319A572BC995A0EB38C1D5FC25D19D1AFEEE7","parts":{"hash":"1193A2D6E9E4610A299AD2DBB167BAFE2C64941A1C21C957094715077582797F","total":1}},"height":5,"pol_round":-1,"round":0,"signature":"PptznfFEZg0rPYEes+J34e/4q+aJugJfBmmBWtdlPk0UWef+Ud4M0kZJsWdBjXCGtbrNlyhWum4+qhupzWh/Cg==","timestamp":"2022-03-28T07:53:08.430680769Z"}
7:53AM INF received complete proposal block hash=A03146F88054588E4AC49B14C7D319A572BC995A0EB38C1D5FC25D19D1AFEEE7 height=5 module=consensus
7:53AM INF finalizing commit of block hash=A03146F88054588E4AC49B14C7D319A572BC995A0EB38C1D5FC25D19D1AFEEE7 height=5 module=consensus num_txs=0 root=E32197103ADE095165B5CDD8027F06CBC83D1F32178232D6CA1F114F607FE75A
7:53AM INF minted coins from module account amount=126ujunox from=mint module=x/bank
7:53AM INF executed block height=5 module=state num_invalid_txs=0 num_valid_txs=0
7:53AM INF commit synced commit=436F6D6D697449447B5B34332032303020363320313420363920323134203132322031313720313033203233382031383720373220353320313435203832203630203235312031383420323720343220323130203232342032392032313420333220313638203135382031393520323120393520313339203130395D3A357D
7:53AM INF committed state app_hash=2BC83F0E45D67A7567EEBB483591523CFBB81B2AD2E01DD620A89EC3155F8B6D height=5 module=state num_txs=0
7:53AM INF indexed block height=5 module=txindex
```

There's a lot to digest here, but the most important thing for now is that the node is producing blocks every few seconds.

> **TIP**💡 The node data is located under the directory `/workspace/.juno`, which will survive Gitpod workspace restart. If you're using local docker container, it is located under the directory `/root/.juno`.

With the node running, run `junod query bank` in **another terminal** to show the account balances (of the `default` account).
We use the command `junod keys show` to obtain the address of the `default` account. The `$()` syntax expands to the output of the quoted command line.

```sh
junod query bank balances $(junod keys show default -a)
```

As you can see, the `default` account is pre-funded with some UJUNOX tokens for us to pay fees. (We may ignore the UCOSM token for now.)


```yaml
balances:
- amount: "1000000000"
  denom: ucosm
- amount: "1000000000"
  denom: ujunox
pagination:
  next_key: null
  total: "0"
```


> **TIP**💡 To stop the testing node, press `Ctrl-C` in the terminal where it is running.

You're now ready to move on to deploy your first CosmWasm smart contract!

