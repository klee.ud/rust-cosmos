Working with native modules
===

Smart contracts alone would not do anything useful without the functionalities
provided by the underlying native modules.

Among the many built-in modules, the `bank` module is one of the most important
ones to smart contract developers, as it is responsible for managing the native
tokens. If you need to update or query user balances, you need to interact with
the `bank` module.

In CosmWasm, we use `BankMsg` and `BankQuery` to communicate with the `bank` 
module.


Querying balances
---

As previously introduced, query message, or query for short, is a way to request
chain states from other SDK modules. Queries are executed **synchronously** 
against a snapshot of chain state just before the execution of a message.

Instead of using the low-level API to make queries, we use the higher-level
`Querier` interface for querying balances. For example, to query the balance
of the message sender, we may do this:

```rust
    // balance is of type Coin
    let balance = deps.querier.query_balance(info.sender.clone(), "ujunox")?;
```

> **TIP**💡 You may read the [source code](https://github.com/CosmWasm/cosmwasm/blob/v1.0.0/packages/std/src/traits.rs#L200)
of the underlying implementation to see how `BankQuery` is being used under the
hood.

> **TIP**💡 You may have noticed the use of the `?` operator in several places
in the contract. It is used to skip error handling and leave that to the callers
further up in the call stack. Learn more about it in the 
[Error Handling](https://doc.rust-lang.org/book/ch09-02-recoverable-errors-with-result.html#a-shortcut-for-propagating-errors-the--operator)
chapter of the Rust book.

In CosmWasm, balances are represented with the `Coin` type, which consists of
an amount and a denomination. The `query_balance` method returns the balance of
a given denomination. Unlike Ethereum, Cosmos blockchains support native tokens
of various denominations, and each token may be used for different purposes. To
obtain the balances of all denominations a given address owns, use the 
`query_all_balances` method:

```rust
    // balances is of type Vec<Coin>
    let balances = deps.querier.query_all_balances(info.sender.clone())?;
```

This method is in fact the programmatic version of the `junod query bank balances` 
CLI command introduced in [Part 2](02-The_first_contract#instantiate-contract).

> **EXERCISE**🥋 Restrict the use of the example contract to only users
that have balance of at least 10000ujunox. Revise also the test cases so that
all the tests pass. Then, deploy the new contract and interact with it using
accounts with different balances.

You may do the checking in `execute` as below:

```rust
    let balance = deps.querier
        .query_balance(info.sender.clone(), "ujunox")?;
    if balance.amount < Uint128::from(10000u32) {
        return Err(ContractError::Unauthorized {});
    }
```

If you run `cargo test` now, all test cases should fail, as expected.

Let's fix the unit tests first. Since we added the restriction that a message
sender must have a minimum balance to use the contract, we need a way to mock
it up in the test cases.

In CosmWasm the [testing](https://docs.rs/cosmwasm-std/1.0.0/cosmwasm_std/testing/index.html)
module gives us the tools to mock the various components in our unit tests. Take
a look at the `increment` test case, where the first line reads:

```rust
    let mut deps = mock_dependencies_with_balance(&coins(2, "token"));
```

This is how we obtain a mocked version of `deps`, which consists of a mocked
`Storage`, a mocked `Api`, and a mocked `Querier`. It also gives the contract a
little bit initial balance. Then, we can inject fake sender balances through the
mocked `Querier`:

```rust
    deps.querier.update_balance("anyone", coins(10001, "ujunox"));
```

In unit tests, the sender address is not verified, so it could be any arbitrary
string. Just align it with the sender in the mocked `MessageInfo` and it'll be
fine. Applying this to all the message sending test cases should give you all
the green lights:

```sh
running 5 tests
test contract::tests::increment ... ok
test contract::tests::reset ... ok
test contract::tests::increment_by ... ok
test contract::tests::proper_initialization ... ok
test integration_tests::tests::count::count ... FAILED
```

Almost. Let's fix also the integration test. Integration tests provide a way to 
test out multi-contract deployment scenarios under a simulated Cosmos SDK environment. 
They are implemented with [cw-multi-test](https://docs.rs/cw-multi-test/latest/cw_multi_test/) 
instead of using mockups.

> **TIP**💡 The `cw-multi-test` package is still very new and considered as
alpha software. It is being constantly updated and improved so has no API 
stability guarantees.

> **TIP**💡 You may find an introduction of it 
[here](https://docs.cosmwasm.com/docs/1.0/smart-contracts/testing/#integration-testing-with-cw-multi-test).

In the function `mock_app`, the initial balance of `USER` is granted by the
`init_balance` method call on the `router.bank` instance. Change the amount to
a value barely exceed the minimum required. As we hard-coded the denomination 
in the contract we need to update the constant `NATIVE_DENOM` to `"ujunox"` too.

```rust
    .init_balance(
        storage,
        &Addr::unchecked(USER),
        vec![Coin {
            denom: NATIVE_DENOM.to_string(),
            amount: Uint128::new(100001),
        }],
    )
```

Run `cargo test` and you'll get an error:

```sh
---- integration_tests::tests::count::count stdout ----
thread 'integration_tests::tests::count::count' panicked at 'called `Result::unwrap()` on an `Err` value: GenericErr { msg: "Querier contract error: Generic error: Invalid input: address not normalized" }', src/contract.rs:46:10
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```

Since this is a simulated environment we cannot use fake addresses or the 
querier will complain. Update the `USER` constant to the address of the 
`default` account and try again. Green lights!

```sh
running 5 tests
test contract::tests::proper_initialization ... ok
test contract::tests::increment ... ok
test contract::tests::increment_by ... ok
test contract::tests::reset ... ok
test integration_tests::tests::count::count ... ok
```


Transferring funds
---

In Solidity, we transfer native tokens by calling the `send` method on a
`payable` object. In CosmWasm, we do the same by sending the `bank` module a
`BankMsg`. However, there is no way to "actively" send messages to a module
from inside a CosmWasm contract. This is one of the major conceptual
difference between CosmWasm and Solidity.

In [Part 5](05-Events.md), we introduced the `Response` object and used it to
emit events. We use it to "send" messages to a module too. In Cosmos blockchains
a transaction consists of one or more messages to be executed by different
modules. The messages are executed in the given order and the transaction is 
only committed when all of them completes successfully. When we return a 
`Response` object with additional messages, they are inserted to the list of 
messages to be executed **after** the current message. You may think of them as
fire-and-forget asynchronous calls that will execute later in the transaction.

We may use the `add_message` or `add_messages` methods to add additional messages 
to the `Response` object, as follows:

```rust
    // add messages by method chaining
    Ok(response
        .add_message(msg1)
        .add_message(msg2))

    // or, add a list of messages
    Ok(response.add_messages(vec![msg1, msg2]))
```

To transfer funds to an account we add a `BankMsg::Send` variant to the 
`Response` object, specifying the amount as a list of `Coin` instances, 
for example:

```rust
    Ok(response
        .add_message(BankMsg::Send {
            to_address: "alice",
            amount: coins(10, "ujunox"),
        }))
```

This tells the `bank` module to send `10ujunox` to `alice`, from the **contract's
own account**. In CosmWasm, each contract owns an account and can only manage
the funds of its own account. Similiar to Solidty, when we send an instantiate
message or an execute message to a contract, we may specify also some funds to
send along with the message, and the funds will be added to the contract's 
account before the message is executed.

The help text of the `junod tx wasm execute` CLI command gives us the hint:

```
Usage:
  junod tx wasm execute [contract_addr_bech32] [json_encoded_send_args] --amount [coins,optional] [flags]
```

So for example if we want to send `10ujunox` and `10ucosm` to the contract we 
may add the `--amount` option after the message:

```sh
junod tx wasm execute $CONTRACT_ADDR '{"increment":{}}' \
  --from default \
  --chain-id testing \
  --gas-prices 0.1ujunox \
  --gas auto \
  --gas-adjustment 1.3 \
  --amount 10ujunox,10ucosm \
  --output json -y
```

In the contract entrypoint we may get the funds sent along with the message
with `info.funds` which is a `Vec<Coin>`. For example, if we want to add 
another requirement that the sender must pay to use the contract, we may add
a checking in `execute` as below:

```rust
    if !has_coins(&info.funds, &Coin::new(100, "ujunox")) {
        return Err(ContractError::PaymentRequired {});
    }
```

Here we used the `has_coins` library function to ensure that the sender has sent
at least `100ujunox` along with the message. Otherwise, we return a custom
error `PaymentRequired`. Here's the code for the custom error, no surprise here:

```rust
    #[error("Payment required")]
    PaymentRequired {},
```

Additionally, we don't want to keep the funds in the contract but instead send
them to the contract owner. To add messages to the `Response` object returned
from the `try_XXX` functions we first store it in a variable (note the use of
the `?` operator):

```rust
    let response = match msg {
        ExecuteMsg::Increment {} => try_increment(deps, 1),
        ExecuteMsg::IncrementBy { amount } => try_increment(deps, amount),
        ExecuteMsg::Reset { count } => try_reset(deps, info, count),
    }?;
```

Then we can add a message to the `Response` object, sending all the funds
received to the owner.

```rust
    Ok(response.add_message(BankMsg::Send {
        to_address: state.owner.to_string(),
        amount: funds,
    }))
```

You have to declare the local variables `state` and `funds` **before** the `match` 
statement:

```rust
    let state = STATE.load(deps.storage)?;
    let funds = info.funds.clone();
```

They have to be declared first or you will run into 
[ownership](https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html) 
problems.


> **EXERCISE**🥋 Restrict the use of the example contract by requiring the 
sender to send at least `100ujunox` along with the message. Revise also the 
test cases so that all the tests pass. Then, deploy the new contract and 
interact with it using an account other than the contract owner. Check the
owner's balance to see if the funds are successfully transferred.

In unit tests, you may set the funds to send with the message by updating
the mocked `MessageInfo`:

```rust
    let info = mock_info("anyone", &coins(100, "ujunox"));
```

But in the integration test, it's a little bit tricky. You need to update
the testing template contract to add a helper function for calling contracts
with some funds:

```rust
    pub fn call_with_funds<T: Into<ExecuteMsg>>(&self, msg: T, funds: Vec<Coin>) -> StdResult<CosmosMsg> {
        let msg = to_binary(&msg.into())?;
        Ok(WasmMsg::Execute {
            contract_addr: self.addr().into(),
            msg,
            funds,
        }
        .into())
    }
```

Then update the test case to replace `call` with `call_with_funds`:

```rust
    let cosmos_msg = cw_template_contract
        .call_with_funds(msg, coins(101, NATIVE_DENOM))
        .unwrap();
```

Run `cargo test` now. Got the green lights? Almost there. Do you know why?

Since the initial balance of the sender is barely above the minimum required, 
and now we are sending `101ujunox` to the contract, so the balance will
drop below the requirement! Fix that and run the tests again. Green lights!

To make the test cases robust, we should also add a test to cover the case that
the sender is not calling the contract with enough funds:

```rust
    #[test]
    fn reset_without_payment() {
        let mut deps = mock_dependencies_with_balance(&coins(2, "token"));

        let msg = InstantiateMsg { count: 17 };
        let info = mock_info("creator", &coins(2, "token"));
        let _res = instantiate(deps.as_mut(), mock_env(), info, msg).unwrap();

        let auth_info = mock_info("creator", &coins(1, "ujunox"));
        deps.querier.update_balance("creator", coins(100001, "ujunox"));
        let msg = ExecuteMsg::Reset { count: 5 };
        let res = execute(deps.as_mut(), mock_env(), auth_info, msg);
        match res {
            Err(ContractError::PaymentRequired {}) => {}
            _ => panic!("Must return payment required error"),
        }
    }
```

And, to check that a `BankMsg` is correctly added to the returned `Response`,
we may use the `assert_eq!` macro:

```rust
    let response = execute(deps.as_mut(), mock_env(), info, msg).unwrap();

    assert_eq!(1, response.messages.len(), "it should have 1 message added");
    assert_eq!(
        SubMsg::new(BankMsg::Send{
            to_address: "creator".to_string(),
            amount: coins(100, "ujunox"),
        }),
        response.messages[0],
        "incorrect message added to response",
    );
```

We have to check the `messages` field of the `Response` directly in the unit
tests since the messages returned will not be processed. On the other hand,
in integration tests, we can query the contract owner's balance after the 
contract call to assert that the correct amount is transferred:

```rust
    let balance = app.wrap()
        .query_all_balances(ADMIN)
        .unwrap();
    assert_eq!(coins(101, NATIVE_DENOM), balance, "incorrect balance");
```

Finally, since we need to query the owner's balance, the dummy address for
`ADMIN` needs to be replaced with a valid address as well. Generate a random
one with `junod keys add`.


> **CHALLENGE**🏆 Try to modify the contract to send exactly `100ujunox` to the
owner instead of all the funds received and return the remaining amounts to the 
sender, possibly including other denominations.



Summary
---

We introduced how to query account balances and how to transfer funds in 
contracts using queries and messages. We also walked through the process of
updating test cases to cover new use cases, demonstrating the use the mock 
objects in unit tests and modifying integration tests built with `cw-multi-test`.

Coming up next, we'll see how we can send queries and messages between smart 
contracts, which is the foundation of composing smart contracts to implement
complex dApps.


