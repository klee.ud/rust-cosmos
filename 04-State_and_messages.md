State and messages
===

State
---

Now let's look into the inner workings of the sample contract.

Like smart contracts on Ethereum, CosmWasm smart contracts also have to maintain certain persistent state to function properly.

At the lowest level, application states in a Cosmos SDK-based blockchain are all kept in a data structure known as [Multistore](https://docs.cosmos.network/master/core/store.html#multistore). Within the multistore, there is a list of `KVStore`, each independently managed by different Cosmos SDK modules (e.g. CosmWasm), and is where the application states actually stored. All the smart contract states combined together form the complete application state of the CosmWasm module.

In CosmWasm, the lower level interface to work with the underlying `KVStore` is the `cosmwasm_std::Storage` instance, accessible to all entrypoints of the smart contract as `deps.storage`. For example, in the `instantiate` entrypoint, you will find these two lines of code:

```rust
    set_contract_version(deps.storage, CONTRACT_NAME, CONTRACT_VERSION)?;
    STATE.save(deps.storage, &state)?;
```

Most of the time, however, we do not use the `Storage` instance directly but instead use the higher level abstraction library [cw-storage-plus](https://github.com/CosmWasm/cw-plus/tree/main/packages/storage-plus). It provides two high level abstract type `Item` and `Map`, which allows you to easily manage simple data values and complex nested data structures respectively.

Contract state definitions are by convention defined in the module `state.rs`, optionally bundled with some helper functions. In the sample contract, the only data item is defined as a simple `struct`, as follow:

```rust
pub struct State {
    pub count: i32,
    pub owner: Addr,
}
```

Similar to Solidity, in Rust `struct` is also used for grouping together a bunch of related values. Here, it defines `State` to have a signed 32-bit integer counter and a wallet address of type `Addr`. Following that is a handle for us to save the state to and read the state from `KVStore`:

```rust
pub const STATE: Item<State> = Item::new("state");
```

This line tells CosmWasm that the contract state is to be stored under the unique key "state" and its data type is `State`. With this handle, we can intuitively access the state using the functions `load`, `save` and `update` respectively, as you can find in `contract.rs`:

```rust
let state = STATE.load(deps.storage)?;

STATE.save(deps.storage, &state)?;

STATE.update(deps.storage, |mut state| -> Result<_, ContractError> {
    state.count += 1;
    Ok(state)
})?;
```

We now know how to manipulate the contract states from within the contract, let's see how we can tell the contract to actually make changes on-chain.

Messages
---

CosmWasm smart contracts use different **messages** to communicate with the outside world, including human users and other Cosmos SDK modules.

In [Part 2](02-The_first_contract.md) we demonstrated how to work with the deployed contract using the `junod` CLI, which involved some JSON snippets. Those snippets are the serialized form of the various **messages** defined in the module `msg.rs`:

```rust
pub struct InstantiateMsg {
    pub count: i32,
}
// {"count":0}

pub enum ExecuteMsg {
    Increment {},
    Reset { count: i32 },
}
// {"increment":{}},{"reset":{"count":42}}

pub enum QueryMsg {
    GetCount {},
}
// {"get_count":{}}

pub struct CountResponse {
    pub count: i32,
}
// {"count":0}
```

> For brevity, the `#[derive()]` **attributes** are not shown here. You may think of the `#[derive()]` attributes as statements that bring in **mixins** (Ruby) or **traits** (PHP), where additional behaviors (e.g. serialization) are attached to the user-defined types. They are crucial to the proper functioning of the contract.
>
> **TIP**💡 Learn more about [traits](https://doc.rust-lang.org/book/ch10-02-traits.html) in the Rust book. A list of built-in [derivable traits](https://doc.rust-lang.org/book/appendix-03-derivable-traits.html) is available in the appendix.


Messages are defined using `struct` or `enum`. Both keywords are used to define complex data types in Rust. In particular, execute messages and query messages are commonly defined using `enum` to leverage the **pattern matching** feature of the Rust language.

> **TIP**💡 Learn more about [struct](https://doc.rust-lang.org/book/ch05-00-structs.html) and [enum](https://doc.rust-lang.org/book/ch06-00-enums.html) in the Rust book.

Once defined, we can specify the type of messages that the contract entrypoints are expected to receive, e.g. in the `instantiate` entrypoint:

```rust
pub fn instantiate(
    ...,
    msg: InstantiateMsg,
) -> Result<Response, ContractError> {
    let state = State {
        count: msg.count,
        owner: info.sender.clone(),
    };
    ...
```

In the `execute` entrypoint, you can see how control flow is handled using **pattern matching**:

```rust
pub fn execute(
    ...
    msg: ExecuteMsg,
) -> Result<Response, ContractError> {
    match msg {
        ExecuteMsg::Increment {} => try_increment(deps),
        ExecuteMsg::Reset { count } => try_reset(deps, info, count),
    }
}
```

The `match` construct does conditional branching based on the `enum` **variant** and also **destructures** its fields into **local variables** scoped only to that branch.

> "Why there's no `return` statement in the function?" You may ask.
>
> In Rust, the value of a block is the value of the last evaluated **expression**, which in the above function is the `match` construct, and the value of the `match` construct is the last expression evaluated for the matching `enum` **variant**.
>
> **TIP**💡 Learn more about [statements and expressions](https://doc.rust-lang.org/book/ch03-03-how-functions-work.html#statements-and-expressions) and [match](https://doc.rust-lang.org/book/ch06-02-match.html) in the Rust book.


In the function `try_increment`, the integer counter is incremented and persisted using the `update` function on the `STATE` handle:

```rust
pub fn try_increment(deps: DepsMut) -> Result<Response, ContractError> {
    STATE.update(deps.storage, |mut state| -> Result<_, ContractError> {
        state.count += 1;
        Ok(state)
    })?;

    Ok(Response::new().add_attribute("method", "try_increment"))
}
```

Finally it signals to CosmWasm that the message is successfully processed by returning `Ok()`, a common idiom in Rust programming. Together with it is the `Response` object that is used for emitting **events** and sending additional messages. We will cover both topics later in the series.

On the other hand, if we want to signal to CosmWasm that the processing is failed or rejected, we would return `Err()` instead with a custom error object as demonstrated in the function `try_reset`:

```rust
pub fn try_reset(deps: DepsMut, info: MessageInfo, count: i32) -> Result<Response, ContractError> {
    STATE.update(deps.storage, |mut state| -> Result<_, ContractError> {
        if info.sender != state.owner {
            return Err(ContractError::Unauthorized {});
        }
...
```

> **TIP**💡 Learn more about [error handling](https://doc.rust-lang.org/book/ch09-02-recoverable-errors-with-result.html) in general in the Rust book.



In the `query` entrypoint, the handling is similar:

```rust
pub fn query(..., msg: QueryMsg) -> StdResult<Binary> {
    match msg {
        QueryMsg::GetCount {} => to_binary(&query_count(deps)?),
    }
}
```

The difference with `execute` is the return type of the function. Instead of returning a `Response`, the `query` function returns a `Binary`, which is the binary encoding of the result. In the case of `GetCount`, it is the `CountResponse` message.



Practice
---

Let's try to enhance the sample contract a little bit.

Suppose we need to add a new message `IncrementBy` that allows us to increment the counter by a given amount. Before proceeding, make sure that we start with a clean working code base. As usual, we run the test cases to confirm that:

```sh
cargo test
```

You should see output like the following:

```
   Compiling proc-macro2 v1.0.28
   Compiling unicode-xid v0.2.2
   Compiling syn v1.0.74
...
   Compiling hello-juno v0.1.0 (/workspace/hello-juno)
    Finished test [unoptimized + debuginfo] target(s) in 52.07s
     Running unittests (target/debug/deps/hello_juno-7ccba05590861468)

running 4 tests
test contract::tests::proper_initialization ... ok
test contract::tests::increment ... ok
test integration_tests::tests::count::count ... ok
test contract::tests::reset ... ok

test result: ok. 4 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s

   Doc-tests hello-juno

running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
```

All tests passed.

First, add a new message `IncrementBy` in `msg.rs` as follows:

```rust
pub enum ExecuteMsg {
    Increment {},
    IncrementBy { amount: i32 },
    Reset { count: i32 },
}
```

At this point `contract.rs` should fail to compile, as indicated by the IDE. The `match` construct in `execute` function is missing the new message variant. So we add it as follows:

```rust
match msg {
    ExecuteMsg::Increment {} => try_increment(deps, 1),
    ExecuteMsg::IncrementBy { amount } => try_increment(deps, amount),
    ExecuteMsg::Reset { count } => try_reset(deps, info, count),
}
```

Note that we changed the `try_increment` call for `Increment` message as well. We want to extend the functionality of `try_increment` to support the new requirement. To do so, add a new parameter `amount` to `try_increment` and use it to update the counter:

```rust
pub fn try_increment(deps: DepsMut, amount: i32) -> Result<Response, ContractError> {
    STATE.update(deps.storage, |mut state| -> Result<_, ContractError> {
        state.count += amount;
        Ok(state)
    })?;

    Ok(Response::new().add_attribute("method", "try_increment"))
}
```

Try run `cargo wasm` now, it should compile without any errors:

```sh
/workspace/hello-juno$ RUSTFLAGS="-C link-arg=-s" cargo wasm

   Compiling hello-juno v0.1.0 (/workspace/hello-juno)
    Finished release [optimized] target(s) in 4.46s
```

> **TIP**💡 The use of `RUSTFLAGS` above is an [alternative method](02-The_first_contract.md#alternative-method) to produce optimized binary for local deployment.

We are not done yet, however. After adding the new functionality we have to add a covering test case. In CosmWasm, unit tests are commonly defined in the same contract source file inside a nested module with the `#[cfg(test)]` attribute. Individual test cases are in turn marked with the `#[test]` attribute. Use the existing `increment` test case as a template and add a new test `increment_by`:

```rust
#[test]
fn increment_by() {
    let mut deps = mock_dependencies_with_balance(&coins(2, "token"));

    let msg = InstantiateMsg { count: 17 };
    let info = mock_info("creator", &coins(2, "token"));
    let _res = instantiate(deps.as_mut(), mock_env(), info, msg).unwrap();

    // beneficiary can release it
    let info = mock_info("anyone", &coins(2, "token"));
    let msg = ExecuteMsg::IncrementBy { amount: 3 };
    let _res = execute(deps.as_mut(), mock_env(), info, msg).unwrap();

    // should increase counter by 1
    let res = query(deps.as_ref(), mock_env(), QueryMsg::GetCount {}).unwrap();
    let value: CountResponse = from_binary(&res).unwrap();
    assert_eq!(20, value.count);
}
```

Then run the tests again:

```sh
/workspace/hello-juno$ cargo test
   Compiling hello-juno v0.1.0 (/home/kennethlee/Sources/cosmwasm/hello-juno)
    Finished test [unoptimized + debuginfo] target(s) in 2.46s
     Running unittests (target/debug/deps/hello_juno-7ccba05590861468)

running 5 tests
test contract::tests::increment ... ok
test contract::tests::reset ... ok
test contract::tests::proper_initialization ... ok
test contract::tests::increment_by ... ok
test integration_tests::tests::count::count ... ok

test result: ok. 5 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
```

We have successfully enhanced the contract to support a new message type! As an exercise, try to go through the process of contract deployment as introduced in [Part 2](02-The_first_contract.md) for the updated contract and send it an `increment_by` message with `junod` CLI.


Summary
---

- We introduced how on-chain contract states are managed in CosmWasm contracts, how they are defined, and how to work with them using `cw-storage-plus`.
- We introduced how messages are used to communicate with CosmWasm contracts, how they are defined, and how they are handled in the contract entrypoints.
- We made a trivial enhancement to the sample contract and also added a covering test case.
