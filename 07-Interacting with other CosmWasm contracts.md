Interacting with other CosmWasm contracts
===

In [Part 6](./06-Working%20with%20native%20modules.md), we demonstrated how to 
use native tokens in smart contracts using the `bank` module. Now, suppose the 
requirement is changed to consume fungible tokens instead. In CosmWasm, the 
fungible token standard is `CW20`, which is modeled after the Ethereum 
standard `ERC20`.

Setup
---

A basic implementation of `CW20` can be found in the 
[cw-plus](https://github.com/CosmWasm/cw-plus/tree/main/contracts/cw20-base) 
repository. Instead of compiling it ourselves, we can download the compiled
WASM binary directly from 
[this link](https://github.com/CosmWasm/cw-plus/releases/download/v0.13.4/cw20_base.wasm).

> **EXERCISE**🥋 Store the downloaded `CW20` binary on the local chain. Save
the code ID for later use. You may want to revisit [Part 2](./02-The_first_contract.md)
for the steps.

To issue a new `CW20` token, we have to first instantiate it, similar to `ERC20`.
Let's figure out how. You already have the code ID, and the missing piece is the 
instantiate message. What does it look like? From where can you find that information?
Where is the messages commonly defined in? Yes right, `src/msg.rs`!

Take a look at [src/msg.rs](https://github.com/CosmWasm/cw-plus/blob/v0.13.4/contracts/cw20-base/src/msg.rs)
in the `cw20-base` repository. You will find the `InstantiateMsg` struct easily:

```rust
pub struct InstantiateMsg {
    pub name: String,
    pub symbol: String,
    pub decimals: u8,
    pub initial_balances: Vec<Cw20Coin>,
    pub mint: Option<MinterResponse>,
    pub marketing: Option<InstantiateMarketingInfo>,
}
```

There're six properties we may provide to instantiate a `CW20` token, two of 
them optional. If you're familiar with `ERC20`, you should immediately recognize
the similarity. Name, symbol and decimals are also mandatory properties of 
an `ERC20` token. The `initial_balances` field, as you may have noticed,
is a `Vec<Cw20Coin>`. `Cw20Coin` is a struct consisting of an address and an
amount:

```rust
pub struct Cw20Coin {
    pub address: String,
    pub amount: Uint128,
}
```

> **TIP**💡 More types can be found in the `cw20` base package [here](https://github.com/CosmWasm/cw-plus/tree/v0.13.4/packages/cw20).


In other words, the field allows us to setup the initial balances for
**multiple accounts** at once. The optional `mint` field is used
to control who can mint new tokens and optionally provide a cap, while the 
optional `marketing` field is used to keep information that are mainly used
by user interfaces, like logo URL, project description, etc. For simplicity
we will skip these optional properties in this tutorial.

As a result, our `CW20` instantiate message should be like:

```json
{
  "name": "Hello CW20",
  "symbol": "HELLO",
  "decimals": 6,
  "initial_balances": [{
    "address": "juno16g2rahf5846rxzp3fwlswy08fz8ccuwk03k57y",
    "amount": "123456000000"
  }]
}
```

And this will create a new `HELLO` token with `123456.0` total supply, with
you (the `default` keypair) as the only holder. Note that the `amount` field
is a `Uint128` and you need to include the zeroes for the specified decimals.

> **EXERCISE**🥋 Instatiates the token with the `junod tx wasm` command. You 
may want to revisit [Part 2](./02-The_first_contract.md) for the steps.

You may check your token balance with the below query:

```sh
junod query wasm state smart $TOKEN_ADDRESS \
 '{"balance":{"address":"juno16g2rahf5846rxzp3fwlswy08fz8ccuwk03k57y"}}'
```

Where `$TOKEN_ADDRESS` is the address of the `CW20` contract just instantiated.



Modifying the contract
---

Let's modify the example contract to allow it to spend `HELLO` token rather than
native one.

First of all, the contract must know the address of the token in order to spend it.
We may choose to store the token address in the contract states, or simply
hardcode it in the contract. Here, we take the latter approach and hardcode it 
as a constant (replace the value with the address of your deployed contract):

```rust
const HELLO_TOKEN: &str = "juno1wug8sewp6cedgkmrmvhl3lf3tulagm9hnvy8p0rppz9yjw0g4wtqwrw37d";
```

Next, we will modify the `execute` entrypoint. The first thing it does is to
ensure that the sender holds a minimum balance. To get the token balance of
the sender, like querying bank balance, we send a query message to the token
contract. We demonstrated how to query token balance from the CLI before, 
in Rust however, you need to bring in some types from the `cw20` base package.

Open the `cargo.toml` file and add the new dependency:

```toml
[dependencies]
...
cw20 = "0.13.4"
```

You may then import the required types in `contract.rs`:

```rust
use cw20::{Cw20QueryMsg, BalanceResponse};
```

Back to the `execute` function, change the minimum balance check as follows:

```rust
    // keep a copy of sender for later
    let sender: String = info.sender.clone().into();
    let balance: BalanceResponse = deps.querier
        .query_wasm_smart(
            HELLO_TOKEN,
            &Cw20QueryMsg::Balance { address: sender.clone() }
        )?;
    if balance.balance < Uint128::from(10_000_000_000u64) {
        return Err(ContractError::Unauthorized {});
    }
```

We use the `query_wasm_smart` method to send queries to other smart contracts,
with the first parameter being the contract address, followed by the query 
message. Note that when using this method, we need to explicitly specify the
type of the query response. Then, we need to adjust the minimum balance, taking
into consideration for the decimal places of the `CW20` token.

The next checking in `execute` is to ensure that the transaction is sent with
some funds. As we are spending `CW20` token rather than native one, we will no
longer attach funds in a transaction, so this checking can be removed.

Finally, to spend `CW20` tokens of the sender, we send a message to the token
contract to transfer certain amount from the sender to the owner. For this we
need to bring in some more types, and the resulting import lines should be like:

```rust
use cosmwasm_std::{to_binary, Binary, WasmMsg, Deps, DepsMut, Env, Event, MessageInfo, Response, StdResult, Uint128};
...
use cw20::{Cw20QueryMsg, Cw20ExecuteMsg, BalanceResponse};
```

To send execute messages to the token contract, we need the `WasmMsg` and 
`Cw20ExecuteMsg` types. Modify the final expression in `execute` as follows:

```rust
    Ok(response.add_message(WasmMsg::Execute {
        contract_addr: HELLO_TOKEN.into(),
        msg: to_binary(&Cw20ExecuteMsg::TransferFrom {
            owner: sender.clone(),
            recipient: state.owner.to_string(),
            amount: Uint128::from(100_000_000u32),
        })?,
        funds: vec![],
    }))
```

Here, `contract_addr` is where we are sending the message to, `msg` is the 
binary encoded message, and `funds` is a list of native tokens to send along 
with the message. We don't care about native tokens now, so just give it an
empty list. In the end, when the transaction is committed, we expect `100.0` 
tokens to be transferred from the sender to the owner.

Allowance
---

At this point, you may wonder why the contract is allowed to spend tokens from
the sender. The answer is of course, no, the contract is not allowed to spend
any tokens by default. In `ERC20`, there's a concept of allowance, and it is
also defined in the `CW20` spec. If you read the definition of `Cw20ExecuteMsg`
you will find two variants `IncreaseAllowance` and `DecreaseAllowance`, which 
works similar to `increaseAllowance` and `decreaseAllowance` in the OpenZepplin
[ERC20 implementation](https://docs.openzeppelin.com/contracts/4.x/api/token/erc20#ERC20).

To increase allowance for our contract, we first need to deploy it. Note that
you should use **another keypair** to deploy the contract so as to make the 
sender and owner differ. Compile the contract, store it on the local chain and 
instantiate a new instance. 

What about the test cases? They are all broken now! Don't worry, we'll fix
them soon. Let's continue with our new contract first.

After deploying the contract, you may try to send it an `increment` message. It 
should fail with the error below:

```
Error: rpc error: code = InvalidArgument desc = failed to execute message; message index: 0: dispatch: submessages: No allowance for this account: execute wasm contract failed: invalid request
```

We may now increase the allowance for the new contract. The `IncreaseAllowance`
message has the following shape:

```rust
    IncreaseAllowance {
        spender: String,
        amount: Uint128,
        expires: Option<Expiration>,
    },
```

The optional `expires` field is used to set a deadline for the allowance. Let's
leave it out now and just provide the spender and an amount of `9999.0`:

```sh
junod tx wasm exec \
 $HELLO_TOKEN \
 '{"increase_allowance":{"spender":"'$COUNTER'","amount":"9999000000"}}' \
 $FROM $GAS -y
```

To query all the allowances you approved, you may use the `all_allowances`
query:

```sh
junod query wasm state smart $HELLO_TOKEN \
 '{"all_allowances":{"owner":"juno16g2rahf5846rxzp3fwlswy08fz8ccuwk03k57y"}}'
```

Having the allowance approved, we way now try to increment the counter again.
This time it should succeed and your token balance should have deducted
by `100.0`, while the owner of the counter contract should have received the
same amount.

Test cases
---

Now let's fix our broken tests. Remind that normally we should make sure that
our tests all pass before deploying to a real chain for testing.

Before, we have used the `update_balance` method on `deps.querier` to setup
the minimum bank balance before executing the contract. For testing against
CosmWasm contracts, we have to use instead the `update_wasm` method that 
sets up a handler that return mocked response to contract queries.

In our case, we only have to mock the token balance of the sender, so for 
example we can add this simple helper that creates a handler that only
respond to balance queries:

```rust
fn mock_cw20_balance(owner: String, balance: Uint128) -> impl Fn(&WasmQuery) -> QuerierResult {
    move |query| {
        if let WasmQuery::Smart { contract_addr, msg } = query {
            let decoded: Cw20QueryMsg = from_binary(msg).unwrap();
            if let Cw20QueryMsg::Balance { address } = decoded {
                let mut result: BalanceResponse = BalanceResponse { balance: Uint128::zero() };
                if address == owner && HELLO_TOKEN == contract_addr {
                    result.balance = balance;
                }
                return SystemResult::Ok(ContractResult::Ok(to_binary(&result).unwrap()))
            }
        }
        SystemResult::Err(SystemError::UnsupportedRequest { kind: "unexpected query".into() })
    }
}
```

Then we can use it to mock the token balance of the sender as follows:

```rust
deps.querier.update_wasm(
  mock_cw20_balance(
    "anyone".into(),
    Uint128::from(10_000_000_000u64)
  )
);
```

Next we check that the returned response contains a message for token transfer:

```rust
assert_eq!(
    SubMsg::new(WasmMsg::Execute {
        contract_addr: HELLO_TOKEN.into(),
        msg: to_binary(&Cw20ExecuteMsg::TransferFrom {
            owner: "anyone".into(),
            recipient: "creator".into(),
            amount: Uint128::from(100_000_000u64),
        }).unwrap(),
        funds: vec![],
    }),
    response.messages[0],
    "incorrect message added to response",
);
```

Finally, as we no longer raise the `PaymentRequired` custom error, the test
case for `reset_without_payment` can be removed.

> **EXERCISE**🥋 Update the unit tests and make them all pass.


With all the green lights from unit tests, we want to test our contracts at
a higher level - interacting with other contracts.

Our contract requires a working CW20 implementation for testing so we need to 
add `cw20-base` as a dev dependency. Add it to `Cargo.toml` as follows:

```toml
[dev-dependencies]
...
cw20-base = { version = "0.13.4", features = ["library"] }
```

In `integration_tests.rs`, add the following helper function that creates a 
wrapper for the real CW20 contract:

```rust
pub fn contract_cw20() -> Box<dyn Contract<Empty>> {
    let contract = ContractWrapper::new(
        cw20_base::contract::execute,
        cw20_base::contract::instantiate,
        cw20_base::contract::query,
    );
    Box::new(contract)
}
```

Then in `proper_instantiate`, we need to store and instantiate it just like
when we're working on the local chain:

```rust
let cw20_id = app.store_code(contract_cw20());
let cw20_contract_addr = app
    .instantiate_contract(
        cw20_id,
        Addr::unchecked(ADMIN),
        &cw20_base::msg::InstantiateMsg {
            name: "Testing Token".into(),
            symbol: "TEST".into(),
            decimals: 6,
            initial_balances: vec![
                Cw20Coin { address: USER.into(), amount: Uint128::from(10_000_000_000u64) },
            ],
            mint: None,
            marketing: None,
        },
        &[],
        "test",
        None,
    )
    .unwrap();
```

Now there's a problem. As we chose to hardcode the token address in our
contract, we can't make it work here with a dynamically deployed instance.
Therefore, we have to enhance our contract a bit to make it more testable.

> **EXERCISE**🥋 Update the contract to store the token address in contract
states instead of hardcoding it.

The new instantiate message should now require an additional field for the
token address:

```rust
let msg = InstantiateMsg {
    count: 1i32,
    token_address: cw20_contract_addr.to_string(),
};
```

You also need to add the contract address to the returned tuple as we need it
in test cases.

In `count`, first we need to add allowance for our testing contract, and then
update the assertion for the ADMIN balance after the contract call. Here's an
example for your reference:

```rust
fn count() {
    let (mut app, cw_template_contract, token_address) = proper_instantiate();

    // add allowance for the testing target
    app.execute(
        Addr::unchecked(USER), 
        WasmMsg::Execute {
            contract_addr: token_address.clone(), 
            msg: to_binary(&Cw20ExecuteMsg::IncreaseAllowance {
                  spender: cw_template_contract.addr().into(), 
                  amount: Uint128::from(10_000_000_000u64), 
                  expires: None,
            }).unwrap(),
            funds: vec![]
        }.into()
    ).unwrap();

    let msg = ExecuteMsg::Increment {};
    let cosmos_msg = cw_template_contract.call(msg).unwrap();
    app.execute(Addr::unchecked(USER), cosmos_msg).unwrap();

    let balance: BalanceResponse = app.wrap()
        .query_wasm_smart(
            token_address,
            &Cw20QueryMsg::Balance { address: ADMIN.into() },
        )
        .unwrap();
    assert_eq!(Uint128::from(100_000_000u64), balance.balance, "incorrect balance");
}
```


Summary
---

Working with other smart contracts is just similar to working with native modules.
To query contract states, we use the `query_wasm_smart` method to send messages
to the contract. And to modify contract states, we add the execute message to
the response wrapped in a `WasmMsg::Execute` enum variant.

In unit tests, we use `update_wasm` to setup handlers to return mock responses 
to contract queries. In integration tests, we use `ContractWrapper` to wrap
dependent contracts and then store and instantiate them as if we are working
on real chains.

You are now equipped with basic skills of coding simple smart contracts. Before
moving on to more advanced topics, we will explore a JS library that help us
build user interfaces and automation scripts for our smart contracts.
