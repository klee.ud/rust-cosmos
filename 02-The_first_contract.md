The first contract
===

Now change the working directory to `/workspace`. We use the `cargo generate` command to create a skeleton CosmWasm contract project. Let's name it `hello-juno`.

```sh
cd /workspace
cargo generate --git https://github.com/CosmWasm/cw-template.git --name hello-juno
```

You should see something similar.

```
⚠️   Unable to load config file: /workspace/.cargo/cargo-generate.toml
🔧   Generating template ...
[ 1/36]   Done: .cargo/config
[ 2/36]   Done: .cargo
[ 3/36]   Skipped: .circleci/config.yml
[ 4/36]   Done: .circleci
...
[33/36]   Done: src/lib.rs
[34/36]   Done: src/msg.rs
[35/36]   Done: src/state.rs
[36/36]   Done: src
🔧   Moving generated files into: `/workspace/hello-juno`...
💡   Initializing a fresh Git repository
✨   Done! New project created /workspace/hello-juno
```

Try to build it for the first time. We use the command `cargo wasm` to build a CosmWasm project.

```sh
cd hello-juno
cargo wasm
```

You should see something similar.

```
    Updating crates.io index
  Downloaded cfg-if v1.0.0
  Downloaded hex v0.4.3
  Downloaded opaque-debug v0.3.0
...
   Compiling cosmwasm-storage v1.0.0-beta7
   Compiling cw2 v0.13.1
   Compiling hello-juno v0.1.0 (/workspace/hello-juno)
    Finished release [optimized] target(s) in 55.55s
```

The compiled contract binary `hello_juno.wasm` is located under the directory `target/wasm-unknown-unknown/release`:

```sh
total 2948
drwxr-xr-x 7 gitpod gitpod     164 Mar 29 04:03 build
drwxr-xr-x 2 gitpod gitpod    4096 Mar 29 04:04 deps
drwxr-xr-x 2 gitpod gitpod       6 Mar 29 04:03 examples
-rw-r--r-- 1 gitpod gitpod     335 Mar 29 04:04 hello_juno.d
-rwxr-xr-x 2 gitpod gitpod 1749978 Mar 29 04:04 hello_juno.wasm
drwxr-xr-x 2 gitpod gitpod       6 Mar 29 04:03 incremental
-rw-r--r-- 1 gitpod gitpod     338 Mar 29 04:04 libhello_juno.d
-rw-r--r-- 2 gitpod gitpod 1247642 Mar 29 04:04 libhello_juno.rlib
```

Great! We may deploy our contract right away. But wait, there's a better way.


Optimized compilation
---

Note that the binary now has 1,749,978 bytes in size. Before deploying it to the chain, we want to optimize the size as much as possible to save gas fees and chain resources. To do that we need `rust-optimizer`. We can use the [published docker image](https://hub.docker.com/r/cosmwasm/rust-optimizer) for this:

```sh
docker run --rm -v "$PWD:/code" -v "$(basename "$PWD")_cache:/code/target" -v registry_cache:/usr/local/cargo/registry cosmwasm/rust-optimizer:0.12.5
```

> **TIP**💡 You have to run the command as-is for it to work. In particular, the project root must be mounted to `/code` inside the container.


You should see something similar.

```
Info: RUSTC_WRAPPER=sccache
Info: sccache stats before build
Compile requests                      0
Compile requests executed             0
Cache hits                            0
...
Failed distributed compilations       0
Cache location                  Local disk: "/root/.cache/sccache"
Cache size                            0 bytes
Max cache size                       10 GiB
done
```

What it does is basically re-compiling the project again **inside** the container and applies bytecode optimizations to the final binary. The named volumes mounted to the container are used to cache dependencies and intermediate files to make subsequent compilations a lot faster as we make incremental changes to the contracts.

The optimized binary is located under the directory `artifacts`:

```sh
total 140
-rw-r--r-- 1 root root    122 Mar 29 07:25 checksums_intermediate.txt
-rw-r--r-- 1 root root     82 Mar 29 07:25 checksums.txt
-rw-r--r-- 1 root root 132572 Mar 29 07:25 hello_juno.wasm
```

As you can see, the optimized binary is shrunk to just 132,572 bytes, that's barely 7.58% of the non-optimized version.

Now take note of these lines in the optimizer output, which shows the SHA-256 checksum of the binary:

```
Creating hashes ...
4cd5b570ad54d0c02f6673d546041d8c7d71dfa060d2e35de4c3744971a0e0c4  hello_juno.wasm
```

A distinguishing feature of `rust-optimizer` is that the artifacts it produces, given the same code base, is **reproducible** on any host. This is a necessary property when we need to do on-chain verification of deployed contracts against publicly disclosed source code.


### Alternative method

For local tests we may also use a Rust compiler flag to produce optimized binaries without Docker:

```sh
RUSTFLAGS='-C link-arg=-s' cargo wasm
```

However, binaries produced this way is not guaranteed to be reproducible on another machine, when deploying to public chains it is recommended to use `rust-optimizer`.


Deploying the contract
---

To deploy a CosmWasm contract, we have to first **store** the contract bytecode and then **instantiate** it.

In CosmWasm, we can create multiple **instances** of the same contract by storing the bytecode on the chain once and then create instances from the bytecode and initialize them with different parameters.

### Store bytecode on-chain

Make sure the testing node is running. To store the bytecode to the chain, we use the command `junod tx wasm store`:

```sh
junod tx wasm store artifacts/hello_juno.wasm \
  --from default \
  --chain-id testing \
  --gas-prices 0.1ujunox \
  --gas auto \
  --gas-adjustment 1.3 \
  --output json -y | jq -r .txhash
```

The `--from` option specifies the named keypair to use to sign the transaction and paying any necessary fees.

The output should look like:

```
gas estimate: 17938196
47D4763CE2949CDF8DD8878BACC62AE37AB51DC8BC5038C58AE9C8E3379D8239
```

> **TIP**💡 The `jq` helper utility is used to extract data from JSON outputs. Here we extracted a string value from the top-level key `txhash`. You can learn more about `jq` [here](https://stedolan.github.io/jq/tutorial/).

The hex string shown above is the `Transaction Hash`. Now we need to find out the `Code ID` in order to instantiate the contract. The `Code ID` is the unique identifier for the bytecode we just stored on-chain. Here, we use the command `junod query tx`:

```sh
junod query tx 47D4763CE2949CDF8DD8878BACC62AE37AB51DC8BC5038C58AE9C8E3379D8239 --output json | jq -r '.logs[0].events[-1].attributes[0]'
```

You should get the JSON fragment below:

```json
{
  "key": "code_id",
  "value": "1"
}
```

### Instantiate contract

We use the command `junod tx wasm instantiate` to create a new instance of our contract:

```sh
junod tx wasm instantiate 1 '{"count":0}' \
  --label 'hello juno' \
  --from default \
  --chain-id testing \
  --gas-prices 0.1ujunox \
  --gas auto \
  --gas-adjustment 1.3 \
  --output json -y | jq -r .txhash
```

The first argument here is the `Code ID` we obtained in the previous step. The following JSON argument is the **initialization message** for the contract, which is equivalent to constructor arguments in Solidity parlance.

Again you should get a transaction hash:

```
gas estimate: 170024
E136CE048CE22514B6130E1867208B6F1DF8083570B3A74A779B8D231F76D3CE
```

We use `junod query tx` again to find out the deployed contract address:

```sh
junod query tx E136CE048CE22514B6130E1867208B6F1DF8083570B3A74A779B8D231F76D3CE \
  --output json | jq -r '.logs[0].events[0].attributes'
```

You should get the JSON fragment below (the address may vary):

```json
[
  {
    "key": "_contract_address",
    "value": "juno14hj2tavq8fpesdwxxcu44rty3hh90vhujrvcmstl4zr3txmfvw9skjuwg8"
  },
  {
    "key": "code_id",
    "value": "1"
  }
]
```

You can also list all the contracts instantiated by `Code ID` by using the command `junod query wasm list-contract-by-code`:

```sh
junod query wasm list-contract-by-code 1 \
  --output json | jq -r .contracts
```

You should get the JSON fragment below (the address may vary):

```json
[
  "juno14hj2tavq8fpesdwxxcu44rty3hh90vhujrvcmstl4zr3txmfvw9skjuwg8"
]
```

Finally, you may query contract info by using the command `junod query wasm contract`:

```sh
CONTRACT_ADDR=juno14hj2tavq8fpesdwxxcu44rty3hh90vhujrvcmstl4zr3txmfvw9skjuwg8
junod query wasm contract $CONTRACT_ADDR --output json
```

You should get the JSON fragment below (the address may vary):

```json
{"address":"juno14hj2tavq8fpesdwxxcu44rty3hh90vhujrvcmstl4zr3txmfvw9skjuwg8","contract_info":{"code_id":"1","creator":"juno16g2rahf5846rxzp3fwlswy08fz8ccuwk03k57y","admin":"","label":"hello juno","created":null,"ibc_port_id":"","extension":null}}
```

Now check the account balance again:

```sh
junod query bank balances $(junod keys show default -a)
```

We see that some fees were paid to deploy our contract.

```yaml
balances:
- amount: "1000000000"
  denom: ucosm
- amount: "998189177"
  denom: ujunox
pagination:
  next_key: null
  total: "0"
```

With the contract deployed, let's try interacting with it!



Interacting with the contract
---

The skeleton contract merely maintains a simple integer counter that increments by one when invoked. Remember that we just initialized it with the JSON fragment below?

```json
{"count":0}
```

To get the current contract state, we use the command `junod query wasm contract-state`:

```sh
junod query wasm contract-state smart $CONTRACT_ADDR '{"get_count":{}}'
```

And the output is like:

```yaml
data:
  count: 0
```

> **TIP**💡 By now you should have noticed that `junod` defaults to display output as YAML, and you can change the format with the option `--output json` (or `-o json` for short.)

> **TIP**💡 You can make `junod` to display output as JSON by default, by running `junod config output json`.


Here, `get_count` is a **query message** defined by the skeleton contract. Different contracts may define different query message for external parties to examine the contract states.

Let's invoke the contract once to increment the counter. To do that we use the command `junod tx wasm execute`:

```sh
junod tx wasm execute $CONTRACT_ADDR '{"increment":{}}' \
  --from default \
  --chain-id testing \
  --gas-prices 0.1ujunox \
  --gas auto \
  --gas-adjustment 1.3 \
  --output json -y | jq -r .txhash
```

And the output is like:

```
gas estimate: 141749
19AE97BDC473E6575FC00A1B33E6FD95E45B71CFA77115BE0561F684AADEBC0D
```


Here, `increment` is an **execute message** defined by the skeleton contract that takes no arguments. Different contracts may define different execute message for external parties to mutate the contract states.


Now let's check again the contract state:

```sh
junod query wasm contract-state smart $CONTRACT_ADDR '{"get_count":{}}'
```

And the output is like:

```yaml
data:
  count: 1
```

It works! You may try invoking the contract a few more times and examine the contract state.

In addition to `increment`, the skeleton contract also provides a way to reset the counter to an arbitrary (signed 32-bit integer) value. Let's reset the counter to 42.


```sh
junod tx wasm execute $CONTRACT_ADDR '{"reset":{"count":42}}' \
  --from default \
  --chain-id testing \
  --gas-prices 0.1ujunox \
  --gas auto \
  --gas-adjustment 1.3 \
  --output json -y | jq -r .txhash
```


And check again the contract state:

```sh
junod query wasm contract-state smart $CONTRACT_ADDR '{"get_count":{}}'
```

And the output is like:

```yaml
data:
  count: 42
```

Here, `reset` is another execute message that takes an additional argument `count`.

At a very high level, execute messages are similar to public non-view functions in a Solidity contract, while query messages are similar to public getter functions.


Summary
---

Now that you have deployed your first contract to a local testing node, let's have a recap of what you have learned so far.

- Add a keypair to keyring with known mnemonic with `junod keys add`.
- List all keypairs in the keyring with `junod keys list`.
- Start a local node with `junod start`.
- Obtain the address of a named keypair with `junod keys show`.
- Check account balances with `junod query bank balances`.
- Create a skeleton CosmWasm contract with `cargo generate`.
- Build a CosmWasm contract with `cargo wasm`.
- Produce a optimized contract binary with `rust-optimizer`.
- Store a contract binary on-chain with `junod tx wasm store`.
- Query info of a transaction with `junod query tx`.
- Instantiate a contract with `junod tx wasm instantiate`.
- List deployed contracts by code ID with `junod query wasm list-contract-by-code`.
- Query info of a contract with `junod query wasm contract`.
- Query contract state with `junod query wasm contract-state`.
- Update CLI config with `junod config`.
- Execute contract with `junod tx wasm execute`.

You should now see a pattern of `junod` commands here:

- `junod keys` manages the keyring.
- `junod config` manages CLI config.
- `junod start` starts a local node.
- `junod query` reads data from the chain.
- `junod tx` modifies state on the chain.

The good news is that what you have just learned are some basic knowledge that actually applies to **any** Cosmos SDK-based blockchains! Simply replace `junod` with the application name of the chain and everything works the same way. For example, on the reference implementation chain `WASM`, you only need to replace `junod` with `wasmd`, as shown in their [introduction](https://docs.cosmwasm.com/docs/1.0/getting-started/interact-with-contract).

On the other hand, if you learned some smart contract related things from other Cosmos SDK-based blockchains, it may as well apply to JUNO too!

We'll now take a quick look of how the skeleton smart contract project is organized, and how to read Rust contract code at a very high level.

