Project organization
===

Open the directory `/workspace/hello-juno` in VSCode. In Gitpod, you can use the hamburger icon (top-left corner), then `File > Open Folder`, or use the shortcut `Ctrl-K Ctrl-O`. The workspace will reload when you click `OK`.

> **TIP**💡 The URL should be changed to https://xxxx.gitpod.io/?folder=/workspace/hello-juno. If you encounter error message during workspace restart, try reloading the page.

Let's take a look at the project structure (some entries are skipped here for clarity.)

Project structure (simplified)
---

```
hello-juno/
|-schema/
| |-count_response.json
| |-execute_msg.json
| |-instantiate_msg.json
| |-query_msg.json
| +-state.json
|-src/
| |-contract.rs
| |-error.rs
| |-lib.rs
| |-msg.rs
| +-state.rs
|-Cargo.lock
+-Cargo.toml
```

### Cargo.toml

First and foremost this is the most important file in the project. It controls how the Rust project is to be compiled and built, defines project metadata, what files are to be included/excluded, what the library dependencies and their versions are, etc. It is functionally equivalent to the `package.json` file in a typical Solidity project.

The companion file `Cargo.lock`, like `package-lock.json`, is the fully resolved dependencies tree, and it should be committed to version control together with `Cargo.toml` when changes are made.

### schema/*.json

This folder contains the JSON schema files used to validate messages sent to or responses returned by the contract.


### src/*.rs

These are the actual Rust contract source files. CosmWasm contracts written in Rust are compiled as **library** code, and the main entry point for Rust libraries is the `lib.rs` file.

> **TIP**💡 For standalone Rust executable projects, the main entrypoint is `main.rs`.

Here's the content of `lib.rs`:

```rust
pub mod contract;
mod error;
pub mod helpers;
pub mod integration_tests;
pub mod msg;
pub mod state;

pub use crate::error::ContractError;
```

Even without any Rust knowledge you should notice there's no program logic defined here.

The sole purpose of `lib.rs` in a typical CosmWasm project is to bring in other source file in the same project (with the `mod` statement) and import symbols from other dependencies to the global scope (with the `pub use` statement.)

The actual contract logic is mainly defined in `contract.rs`. The messages and responses used by the contract are defined in `msg.rs`. The on-chain state managed by the contract is defined in `state.rs`. Contract-defined errors are defined in `error.rs`.

You may skip `helpers.rs` and `integration_tests.rs` for the moment.

Contract entry points
---

The most important entry points in a CosmWasm contract are the functions `instantiate`, `execute` and `query`. There're also some optional entry points a contract can define to implement additional functionalities, which we'll cover later in the series.

```rust
#[cfg_attr(not(feature = "library"), entry_point)]
pub fn instantiate(..., msg: InstantiateMsg) -> Result<Response, ContractError> {

}

#[cfg_attr(not(feature = "library"), entry_point)]
pub fn execute(..., msg: ExecuteMsg) -> Result<Response, ContractError> {

}

#[cfg_attr(not(feature = "library"), entry_point)]
pub fn query(..., msg: QueryMsg) -> StdResult<Binary> {
}
```

Entry points in a CosmWasm contract are all marked with the Rust [attribute](https://doc.rust-lang.org/reference/attributes.html) `#[entry_point]`. The syntax shown above is a conditional way of expressing the same thing. In plain English, it means "apply `#[entry_point]` to the function if compiler feature `library` is not enabled". We'll not delve into details here and you can just use `#[entry_point]` for your contracts.

As you may have guessed, the three entry points correspond to instantiation, contract execution and query respectively. The last function parameter, `msg`, is the Rust representation of the JSON messages sent to the contracts. When messages are **dispatched** to the contract, the JSON messages are validated and deserialized automatically into Rust representations. We will talk more about messages in [Part 4](04-State_and_messages.md).


A closer look
---

The implementation is pretty straightforward to understand even without prior Rust experience. Don't worry as we will demystify all the language syntax and constructs as the tutorial progresses.

### instantiate()

The `instantiate` entry point is responsible for setting contract metadata and initializing the contract state. It ends by returning a successful response and some metadata in the form of **event attributes**.

```rust
    let state = State {
        count: msg.count,
        owner: info.sender.clone(),
    };
    set_contract_version(deps.storage, CONTRACT_NAME, CONTRACT_VERSION)?;
    STATE.save(deps.storage, &state)?;

    Ok(Response::new()
        .add_attribute("method", "instantiate")
        .add_attribute("owner", info.sender)
        .add_attribute("count", msg.count.to_string()))
```

### execute()

The `execute` entry point is responsible to handle all contract execution logic. But as a common pattern in CosmWasm, it usually contains a single `match` block (similar to `switch` statement in other programming languages) to delegate the call to other local functions.

```rust
    match msg {
        ExecuteMsg::Increment {} => try_increment(deps),
        ExecuteMsg::Reset { count } => try_reset(deps, info, count),
    }
```

> **TIP**💡 In Solidity, actually it works similarly under the hood, where the compiled Solidity code does the delegation for you automatically based on the function ID (first 32-bit of the contract call data.)


### query()

The `query` entry point is responsible to handle all contract query logic. Similar to `execute`, it usually contains a single `match` block to delegate the call to other local functions.

```rust
    match msg {
        QueryMsg::GetCount {} => to_binary(&query_count(deps)?),
    }
```


Summary
---

- We went through the basic project structure of a typical CosmWasm smart contract project.
- We introduced the three basic entry points of a CosmWasm project, and walked through the implementation of them at a very high-level.

Hopefully it should have provided you some directions on studying Rust smart contracts written by other developers.

