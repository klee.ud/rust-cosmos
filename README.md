CosmWasm Smart Contract Tutorial
===

This tutorial aims to provide a quick and high-level tutorial for developing **CosmWasm** smart contract on the JUNO chain (and any other smart contract-enabled Cosmos SDK chains in general.)

Prior experience of developing smart contracts in other blockchains, particularly Ethereum-based ones, is assumed.

This tutorial assumes you to follow along under a **Gitpod** workspace, where all the necessary tooling are pre-installed. Sign up a free account [here](https://gitpod.io/) (you need a **Gitlab.com** account as well) and then launch the workspace with [this link](https://gitpod.io/#https://gitlab.com/klee.ud/rust-cosmos/-/tree/dev).

Local Environment
---

To work under a local development environment, the following toolings have to be installed:

- [rust](https://www.rust-lang.org/tools/install)
- [docker](https://docs.docker.com/desktop/)
- [jq](https://stedolan.github.io/jq/download/)

Also, run these commands to install additional Rust toolings:

```sh
rustup target add wasm32-unknown-unknown
cargo install cargo-generate --features vendored-openssl
cargo install cargo-run-script
```

VSCode is recommended for working with contract source code written in Rust.

- [vscode](https://code.visualstudio.com/Download)
- recommended extensions
   - [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=matklad.rust-analyzer)
   - [Even Better TOML](https://marketplace.visualstudio.com/items?itemName=tamasfe.even-better-toml)
   - [crates](https://marketplace.visualstudio.com/items?itemName=serayuzgur.crates)

> **TIP**💡 The `rust-analyzer` extension may cause conflicts with the original Rust extension. It is recommended to disable the Rust extension when using it.

> **TIP**💡 The original Rust extension is no longer being maintained. You should use `rust-analyzer` instead, as it's now the official [recommendation](https://blog.rust-lang.org/2022/02/21/rust-analyzer-joins-rust-org.html).


The following docker images are needed as well:

- [ghcr.io/cosmoscontracts/juno:v9.0.0](https://github.com/CosmosContracts/juno/releases/tag/v9.0.0)
- [cosmwasm/rust-optimizer:0.12.5](https://hub.docker.com/r/cosmwasm/rust-optimizer/tags?page=1&name=0.12.5)

Create a directory under `$HOME` as a workspace for CosmWasm development, for example:

```sh
mkdir $HOME/cosmwasm
```

This directory will be equivalent to the `/workspace` directory mentioned throughout this tutorial.

To start the testing chain locally, run this under a terminal:

```sh
docker run --name juno_test --init \
  -p 127.0.0.1:26657:26657 \
  -p 127.0.0.1:1317:1317 \
  -e STAKE_TOKEN=ujunox \
  -e UNSAFE_CORS=true \
  -v juno_test_data:/root/.juno \
  -v $HOME/cosmwasm:/workspace \
  ghcr.io/cosmoscontracts/juno:v9.0.0 \
  ./setup_and_run.sh juno16g2rahf5846rxzp3fwlswy08fz8ccuwk03k57y
```

This spins up a container named `juno_test` and bind mounts the workspace directory on the host as `/workspace` inside the container.

When you are asked to run `junod` in the tutorial, it has to be run inside the container, for example:

```sh
# for this command in Gitpod terminal
junod keys list

# you need to run this locally instead
docker exec -it -w /workspace juno_test junod keys list
```

Alternately, you may also launch a separate shell for convenience, and run `junod` as if you're working in Gitpod:

```sh
docker exec -it -w /workspace juno_test /bin/sh
/workspace $ junod keys list
```

You may install `jq` inside the running container for convenience:

```sh
apk add jq
```

For commands involving `cargo`, you have to run them on the host, replacing `/workspace` with `$HOME/cosmwasm`:

```sh
cd $HOME/cosmwasm
cargo generate --git https://github.com/CosmWasm/cw-template.git --name hello-juno
```


References
---

Cosmos/CosmWasm

- https://v1.cosmos.network/intro
- https://docs.cosmwasm.com/dev-academy/basics/cosmos-sdk-cw
- https://docs.junonetwork.io/smart-contracts-and-junod-development/getting-started
- https://github.com/InterWasm/cw-template
- https://academy.terra.money/courses/cosmwasm-smart-contracts-i

Rust

- https://doc.rust-lang.org/book/ch00-00-introduction.html
- https://doc.rust-lang.org/stable/rust-by-example/
- https://github.com/rust-lang/rustlings/
- https://www.youtube.com/c/LetsGetRusty
- https://academy.terra.money/courses/rust-basics




