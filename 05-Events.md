Events
===

One of the ways the Cosmos SDK modules communicate with the off-chain world is by using **events**. Naturally, this functionality is also made available to CosmWasm contracts too. Events are similar to those used in Solidity that are used to capture and record contextual transaction information.

Recall that at the end of most entrypoints `Ok()` is returned with a `Response` object, for example:

```rust
    Ok(Response::new()
        .add_attribute("method", "instantiate")
        .add_attribute("owner", info.sender)
        .add_attribute("count", msg.count.to_string()))
```

Here, some additional information is attached to the `Response` object via the `add_attribute` method. These attributes will be added to an event named `wasm` that is emitted when the transaction is committed.

We can query the events emitted by a transaction by `junod query tx`:

```sh
junod query tx \
 F52164DC30EF9D7DF4B79E7819E0E023FE4441E8371C2F18BE4B6767936E996F \
 -o json | jq .
```

The events emitted can be found in the array at path `.logs[0].events`. We can extract just the `wasm` event with `jq`:

```
... | jq '.logs[0].events[] | select(.type=="wasm")'
```

Which shows the attributes added to the `Response` object:

```json
{
  "type": "wasm",
  "attributes": [
    {
      "key": "_contract_address",
      "value": "juno1suhgf5svhu4usrurvxzlgn54ksxmn8gljarjtxqnapv8kjnp4nrsf8smqw"
    },
    {
      "key": "method",
      "value": "instantiate"
    },
    {
      "key": "owner",
      "value": "juno16g2rahf5846rxzp3fwlswy08fz8ccuwk03k57y"
    },
    {
      "key": "count",
      "value": "0"
    }
  ]
}
```

Alternatively, we can create a custom `cosmwasm_std::Event` object to give the event a different name. The above example can be written as:

```rust
    let event = Event::new("myEvent")
        .add_attribute("method", "instantiate")
        .add_attribute("owner", info.sender)
        .add_attribute("count", msg.count.to_string());

    Ok(Response::new()
        .add_event(event))
```

Events emitted in this way will have the `type` prefixed with the string `"wasm-"`, so for this example the type will be `"wasm-myEvent"`.

> **EXERCISE**🥋 Replace the return value of `instantiate` entrypoint with a custom event and deploy a new contract, then inspect the events emitted.

Applications can subscribe to events through the Tendermint RPC [WebSocket endpoint](https://docs.tendermint.com/master/tendermint-core/subscription.html#legacy-streaming-api) directly, or using the [CosmJS wrapper](https://cosmos.github.io/cosmjs/latest/tendermint-rpc/index.html). However, this method is being deprecated and will be removed in Tendermint version 0.37. It is still available as of Cosmos SDK version 0.45.4 (which is based on Tendermint 0.34) at the time of writing.

